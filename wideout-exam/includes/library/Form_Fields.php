<?php
	/**
     *  InputForm fields Attribute Reference :
     * __construct(@fields)
     *
     * where : 
     *         @fields[@string name]['title'] = @string 
     *         @fields[@string name]['type'] = text | textarea | select | multi_select | hidden | password | checkbox | radio
     *         @fields[@string name]['check'] = numeric|email|required|date|integer|float|not_zero or  any of these combinations separated by |
     *         @fields[@string name]['list'] = @array
     *         @fields[@string name]['blank_option'] = @string
     *         
     **/

class Form_Fields {
    
    protected $_fields              = array(),
              $_fieldHtmlAttribs    = array();
    
    var $_formname,
    	$is_ajax_submit,
    	$base_url,
    	$current_url;
    
    /**
     *
     **/
    public function __construct($formname='input_form', $is_ajax_submit=false ) {
        $this->_formname = $formname;
       	$this->set_ajax_submit($is_ajax_submit);
       	$this->base_url = base_dir_uri();
       	$this->current_url = current_url();
       	
    }
    
    /**
     *
     **/
    public function setFormName($formname) {
        $this->_formname = $formname;
    }

    /**
     *
     **/
    public function getFormName() {
        return $this->_formname;
    }

    /**
     *
     **/
    public function addText($name,$attr=array()) {
        $this->_addField($name,'text',$attr);
    }
    
    /**
     *
     **/
    public function addTextArea($name,$attr=array()) {
        $this->_addField($name,'textarea',$attr);
    }
    
    /**
     *
     **/
    public function addSelect($name,$attr=array()) {
        $this->_addField($name,'select',$attr);
    }
    
    /**
     *
     **/
    public function addMultiSelect($name,$attr=array()) {
        $this->_addField($name,'multi-select',$attr);
    }
    
    /**
     *
     **/
    public function addRadio($name,$attr=array()) {
        $this->_addField($name,'radio',$attr);
    }
    
    /**
     *
     **/
    public function addCheckbox($name,$attr=array()) {
        $this->_addField($name,'checkbox',$attr);
    }
    
    /**
     *
     **/
    public function addCheckboxBoolean($name,$attr=array()) {
        $this->_addField($name,'boolean-checkbox',$attr);
    }    

    /**
     *
     **/
    public function addPassword($name,$attr=array()) {
        $this->_addField($name,'password',$attr);
    }


    /**
     *
     **/
    public function addHidden($name,$attr=array()) {
        $this->_addField($name,'hidden',$attr);
    }
    

    /**
     *
     **/
    public function addSpan($name,$attr=array()) {
        $this->_addField($name,'readonly',$attr);
    }
        
    /**
     *
     **/
    public function setTitle($name,$title) {
        $this->_fields[$name]['title']   = $title; 
    }
    
    /**
     *
     **/
    public function getTitle($name) {
        $title =  $this->_fields[$name]['title'];
        
        if(empty($title)) {
        	$title = str_replace('_', ' ',$name);
        	return  ucwords($title);
        } else {
        	return $title;
        }
    }    
    
    /**
     *
     **/
    public function setValue($name,$value) {
        $this->_fields[$name]['value'] = $value;
    }

    /**
     *
     **/
    public function getValue($name) {
        return $this->_fields[$name]['value'];
    }
    
    /**
     * 
     **/
    public function setAttribs($name,$attr=array()) {
    	foreach($attr as $itemname=>$props) {
    		$this->_fields[$name][$itemname] = $props;	
    	}	
    } 

    /**
     * 
     **/
    public function getAttribs($name) {
    	return $this->_fields[$name];
    } 
    
    /**
     * 
     **/
    public function setAttribsItem($name,$item,$value) {
    	$this->_fields[$name][$item] = $value;		
    } 

    /**
     * 
     **/
    public function getAttribsItem($name,$item) {
    	return $this->_fields[$name][$item];	
    } 
    
    
    /**
     *
     **/
    public function setOptions($name,$value=array()) {
        $this->_fields[$name]['list'] = $value;
    }
    
    /**
     *
     **/
    public function getOptions($name ) {
       return  $this->_fields[$name]['list'] ;
    }
    
    /**
     *
     **/
    public function setValidationCheck($name,$value=array()) {
        $this->_fields[$name]['check'] = $value;
    }
    
    /**
     *
     **/
    public function getValidationCheck($name ) {
       return  $this->_fields[$name]['check'] ;
    }    
    
    /**
     *
     **/
    public function setHtmlAttribs($name,$attr=array()) {
    	foreach($attr as $itemname=>$props) {
        	$this->_fieldHtmlAttribs[$name][$itemname] = $props;
    	}
    }
    
    /**
     *
     **/
    public function getHtmlAttribs($name) {
        return $this->_fieldHtmlAttribs[$name];
    }  

    /**
     *
     **/
    public function setHtmlAttribsItem($name,$item,$value) {
        $this->_fieldHtmlAttribs[$name][$item] = $value;
    }
    
    /**
     *
     **/
    public function getHtmlAttribsItem($name,$item) {
    	return $this->_fieldHtmlAttribs[$name][$item];     
    }    
      
    /**
     * 
     **/
    public function setCheck($name,$callback) {
    	$this->_fields[$name]['check'] = $callback;
    } 
    
    /**
     * 
     **/
    public function getCheck($name) {
    	return $this->_fields[$name]['check'];
    } 
    
    
    /**
     *
     **/
    function _addField($name,$type='text',$attr=array()) {
        $this->_fields[$name]['type'] = $type;
        foreach($attr as $k=>$v) {
            $this->_fields[$name][$k] = $v;
            
        }
    }
    
    /**
     * 
     **/
    public function getFields() {
    	return $this->_fields;		
    }

    /**
     * 
     **/
    public function getFieldHtmlAttribs() {
    	
    	return $this->_fieldHtmlAttribs;
    }
    
    /**
     * 
     **/
    public function is_ajax_submit() {
    	return $this->is_ajax_submit;	
    } 

    /**
     * 
     **/
    public function set_ajax_submit($bln) {
    	$this->is_ajax_submit = $bln;	
    }    
    
    
    /**
     *
     **/
    public function  get_js_validation_code() {
        foreach($this->_fields as $name=>$f) {
        	$checkstr=$this->getCheck($name);
        	$title = strip_tags($this->getTitle($name));
        	
        	if(strpos($checkstr,'numeric') !== FALSE) {
        		$js .= <<<EOS
        			
        		if(document.{$this->getFormName()}.{$name}.value.length > 0) {
        			if(isNaN(document.{$this->getFormName()}.{$name}.value)) {
        				alert("{$title} must be numeric.");
        				return false;        				
        			}
        		}
EOS;

        	}
        	
            if(strpos($checkstr,'required') !== FALSE) {
        		$js .= <<<EOS
        			
        			if(document.{$this->getFormName()}.{$name}.value.length == 0) {
        				alert("{$title} is required.");
        				return false;
        			}
EOS;

        	}     

            if(strpos($checkstr,'float') !== FALSE) {
        		$js .= <<<EOS
        		
        			var test1 = document.{$this->getFormName()}.{$name}.value.toString().indexOf('.')
        			var test2 = document.{$this->getFormName()}.{$name}.value.toString().lastIndexOf('.')
      				var test3 = document.{$this->getFormName()}.{$name}.value.toString().lastIndexOf('.') == -1
        			if((isNaN(document.{$this->getFormName()}.{$name}.value)) || test1 != test2 || test3) {
        				alert("{$title} must be floating point value.");
        				return false;
        			}
EOS;

        	}

            if(strpos($checkstr,'integer') !== FALSE) {
        		$js .= <<<EOS
        		
        			var test1 = document.{$this->getFormName()}.{$name}.value.toString().indexOf('.') != -1
        			var test2 = document.{$this->getFormName()}.{$name}.value.toString().lastIndexOf('-') > 0
        			

        			if((isNaN(document.{$this->getFormName()}.{$name}.value)) || test1 || test2 ) {
        				alert("{$title} must be integer value.");
        				return false;
        			}
EOS;

        	}  

            if(strpos($checkstr,'not_zero') !== FALSE) {
        		$js .= <<<EOS
        		
        			var test1 = {$this->getFormName()}.{$name}.value * 1

        			if( test1 == 0) {
        				alert("{$title} must not be zero.");
        				return false;
        			}
EOS;

        	}

            if(strpos($checkstr,'positive') !== FALSE) {
        		$js .= <<<EOS
        		
        			var test1 = isNaN({$this->getFormName()}.{$name}.value)
        			var test2 = {$this->getFormName()}.{$name}.value * 1 <= 0

        			if( test1 || test2) {
        				alert("{$title} must be a positive number.");
        				return false;
        			}
        			
        			
EOS;

        	}

        	
        }
		
		$js .= <<<EOS
					
				return true;
				
EOS;

       if($this->is_ajax_submit()) {
       		$path = $this->base_url;
       		$retjs= "<script src=\"{$path}www/js/jquery/jquery-1.8.0.js\"></script>";
       } 
        
       $js = <<<EOS
       	
       	<script>
       		
       		function validate_{$this->getFormName()}() {
       		
       		{$js}
       		
       		}
       		       		
       	</script>
EOS;
		
		return $retjs.$js;
    }
    
    /**
     * re-inserts hidden fields, useful when the form is layed out in free form 
     * and the developer forgets to include hidden fields. helpful for ids
     */
    function addHiddenHtml() {  	 

    	$hidden=array(); 
 
    	$ctr=0;
    	foreach($this->_fields as $name=>$attr) {
			$value = $attr['value'];
    		if($attr['type'] == 'hidden') {
 		  		$o = new stdClass();
 		  		$o->id = $name;
 		  		$o->name = $name;
 		  		$o->value = $attr['value'];

 		  		$hidden[] = $o;
    		}
    	} 
 
    	return  $hidden;
    	
    }
    
    /**
     * 
     **/
    public function get_js_ajax_submit_code() {
		$hidden = json_encode($this->addHiddenHtml());
		
		$text1 = $this->is_ajax_submit ? "event.preventDefault();\n" : "\n";
		
		if(!$this->is_ajax_submit) {
			$text2 = <<<EOS
				return true;\n
EOS;
		} else {
			$text2 = <<<EOS
				send_form_ajax();\n
EOS;
		}
 
    	$js_jquery = <<<EOS
    	<script>
    		$(document).ready(function() {
    		
			    // collect multi select selected options
			    var get_multiselect_options = function() {	  
					$.each($('form [multiple]'), function(index,val) {
    					var name = $(this).attr('name') + '_set[]';
    					$(this).parent().find($("[name='"+name+"']")).remove()
    				}); 
			    
		            $.each($('form [multiple]').find(":selected")  , function(index, val) {
		 
		                var name = $(this).parent().attr('name') + '_set[]'
		                $(this).parent().after('<input type="hidden" value="'+ $(this).attr('value') +'" name="'+name+'"/>') 
		                                                                                                                                                                                
		                  
		            });	                      
			    }
			    
			    // add the hidden field with checking if hidden input has already been included in form
			    var hidden = {$hidden} 
				
			    for(var i=0;i<hidden.length;++i) {
			    	//console.log(hidden[i]);
			    	var element = $('form[name="{$this->getFormName()}"]').find('#'+hidden[i].id);
			    	if(element.length > 0) {
			    		$(element).val(hidden[i].value);
			    		 
			    	} else {
			    		var html = '<input type="hidden" id="' + hidden[i].id + '" name="' + hidden[i].id + '" value="' + hidden[i].value +'"/>';
			    		
			    		$('form[name="{$this->getFormName()}"]').append(html);
			    	}
			    	 
			    }
			    
			   
			    
	    		$('form[name="{$this->getFormName()}"]').find('input[type="submit"]').on('click',function(event){
	    				 
		    		// remove default event action for submit button
		    		//event.preventDefault();
		    		{$text1}		    		

		    		//js level validation 
					if(!validate_{$this->getFormName()}()) {
				        return false;
				    }     	 
		    	 	// collect multiselect selected options
		    	 	 get_multiselect_options();
				    
		    	 	 {$text2}
		    	 	 

	    		
	    })
	    
	    function send_form_ajax() {
    		$.ajax({
    			type:"POST",
    			data:$('form[name="{$this->getFormName()}"]').serialize(),
    			dataType:"json",
    			url: "{$this->current_url}",
    			beforeSend:function(xhr) {
    			   $('#{$this->getFormName()}').find('.system-message').html("Saving...")	
    			   $('#{$this->getFormName()}').find('.failed-message').html("")	
    			   $('#{$this->getFormName()}').find('.success-message').html("")	
				   $('#{$this->getFormName()}').find('input[type="text"]').attr('disabled','disabled')
				   $('#{$this->getFormName()}').find('select').attr('disabled','disabled')    				
    			},
    			error:function(xhr) {
    				$('#{$this->getFormName()}').find('failed-message').html(xhr)
				    $('#{$this->getFormName()}').find('input[type="text"]').removeAttr('disabled')
				    $('#{$this->getFormName()}').find('select').removeAttr('disabled')    				
    				
    			},
    			success:function(response) {
    				 //console.log(response.fieldErrors )
    				 try {
	    					if(response.success) {
	    						$('#{$this->getFormName()}').find('.success-message').html(response.success_msg)			    						
	    									    						
	    						if(response.success_url) {
	    							window.location = response.success_url
	    						}
	    						
	    						if(response.callback) {			    							
	    							try {
	    								var cb = response.callback
	    								cb();
	    							} catch(e) {
	    							
	    							} 
	    						}

	    					}
	    					if(response.failed) {
	    						$('#{$this->getFormName()}').find('.failed-message').html(response.failed_msg)
	    					}
	    					if(response.error) {
	    						$('#{$this->getFormName()}-errors').html(response.error)	
	    					}
							$.each(response.fieldErrors,function(index, val) {
    							// console.log(val.key + 'each');
    							//console.log(val.text)
    							
    							if(val.text!=null) {
    								// error message beside the field input		
    								$('form[name="{$this->getFormName()}"]').find("#"+val.key+"-error").html(val.text)

    							}
    						})   
    						
							$('#{$this->getFormName()}').find('.system-message').html("")
							$('#{$this->getFormName()}').find('.field-error').html("")									
						    $('#{$this->getFormName()}').find('input[type="text"]').removeAttr('disabled')
						    $('#{$this->getFormName()}').find('select').removeAttr('disabled') 
 						        						
 
    				} catch(e) {
    				}
    			 	 
    			}
    		})	    
	    }

    
}) 
</script>   
EOS;

    	return $js_jquery;
    }

   
    
 
}

 
 ?>