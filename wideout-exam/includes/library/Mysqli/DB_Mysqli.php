<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
define('FETCH_ROW',0);
define('FETCH_ASSOC',1);
define('FETCH_BOTH', 2);
define('FETCH_OBJECT',3);

class DB_Mysqli  {
	var $last_insert_id,
		$last_error,
		$fetch_mode,
		$host,
		$db,
		$user,
		$password,
		$row_count;
	
	/**
	 * 
	 */
	function __construct($host,$user,$password,$dbname, $log=null) {
		$this->host = $host;
		$this->user = $user;
		$this->password = $password;
		$this->dbname = $dbname;
		$this->log= $log;
		$this->setFetchMode(FETCH_ASSOC);
		
	}	

	/**
	 *  this class needs to be loaded with the controller using load->library()
	 *  get_instance() is used to get the current controller and all resources
	 *  call to get_db_connection() of Controller is done to get the resource object
	 *  in this case, mysqli connection 
	 */
	function connect() {
		try {
			$this->dbconn = new mysqli($this->host,$this->user,$this->password,$this->dbname);			
		} catch(Exception $e) {
			$this->set_error( 'Connection Failed : ' . $e->getMessage()  . ' ' . $e->getFile()  . ' line ' . $e->getLine());
			if(!empty($this->log)) {
				$this->log_error($this->last_error);
			}	
			exit;
		}
				
	}
	
	/**
	 * 
	 */
	function setFetchMode($mode) {
		$this->fetch_mode=$mode;	
	}
	
	/**
	 * 
	 */
	protected function fetch($result, $type='row') {
		if($type == 'row') {				
			switch($this->fetch_mode) {
				case FETCH_ROW:
					return $result->fetch_row();	
				break;
				case FETCH_ASSOC:
					return $result->fetch_assoc();
				break;
				case FETCH_BOTH:
					return $result->fetch_array(MYSQLI_BOTH);
				break;
				case FETCH_OBJECT:
					return $result->fetch_object();
				break;
			}			
		}elseif($type == 'one') {
			$row = $result->fetch_row();
			return $row[0];	
		} else { // all
			switch($this->fetch_mode) {
				case FETCH_ROW: 
					$mode = MYSQLI_NUM;	
				break;
				case FETCH_ASSOC:
					$mode = MYSQLI_ASSOC;
				break;
				case FETCH_BOTH:
					$mode = MYSQLI_BOTH;
				break;
				case FETCH_OBJECT:
					 
				break;
			}			
			
			
			if($this->fetch_mode != FETCH_OBJECT) {
				return $result->fetch_all($mode);
			} else {
				$rowset =$result->fetch_all(MYSQLI_ASSOC);
				
				foreach($rowset as $k=>$v) {
					$o = new stdClass();
					foreach($v as $name=>$value) {
						$o->$name = $value;	
					}	
					$ret[$k] = $o;
				}
				return $ret;
			}
		}		
	} 
	
	/**
	 * 
	 */
	function getAll($query) {
		try {
			
			$this->query_result = $this->dbconn->query($query);
			if($this->query_result)
				return $this->fetch($this->query_result,'all');
			else {
				$strError .= "\n{$this->dbconn->connect_error}";
				$strError .= "\n{$this->dbconn->error}";				
				$this->set_error($strError);
				if(!empty($this->log)) {
					$this->log_error($this->last_error);
				}
			}
		} catch(Exception $e) {
			
			$this->set_error( 'Query Failed : ' . $e->getMessage()  . ' ' . $e->getFile()  . ' line ' . $e->getLine());
			if(!empty($this->log)) {
				$this->log_error($this->last_error);
			}	
						
		}
		return false;
		 	
	}
	
	/**
	 * 
	 */
	function getRow($query) {
		try {
			
			$this->query_result = $this->dbconn->query($query);
			
			if($this->query_result)
				return $this->fetch($this->query_result,'row');
			
			else {
				$strError .= "\n{$this->dbconn->connect_error}";
				$strError .= "\n{$this->dbconn->error}";
				$this->set_error($strError);
				if(!empty($this->log)) {
					$this->log_error($this->last_error);
				}
			}			
			
		} catch(Exception $e) {
			
			$this->set_error( 'Query Failed : ' . $e->getMessage()  . ' ' . $e->getFile()  . ' line ' . $e->getLine());
			if(!empty($this->log)) {
				$this->log_error($this->last_error);
			}	
						
		}	
		return false;	
	}
	
	/**
	 * 
	 */
	function getOne($query) {
		try {
			
			$this->query_result = $this->dbconn->query($query);
			if($this->query_result)
				return $this->fetch($this->query_result,'one');			
			else {
				$strError .= "\n{$this->dbconn->connect_error}";
				$strError .= "\n{$this->dbconn->error}";				
				$this->set_error($strError);
				if(!empty($this->log)) {
					$this->log_error($this->last_error);
				}
			}			
			
		} catch(Exception $e) {
			
			$this->set_error( 'Query Failed : ' . $e->getMessage()  . ' ' . $e->getFile()  . ' line ' . $e->getLine());
			if(!empty($this->log)) {
				$this->log_error($this->last_error);
			}	
						
		}
		return false;		
	}
	
	/**
	 * 
	 */
	function exec($query) {
		
		try {
			$query .= ";";
			$this->query_result = $this->dbconn->query($query);
			
			if($this->query_result) {
				
				$mquery = strtoupper(str_replace('  ', ' ',$query));
			
				$is_add = strpos($mquery, " INSERT INTO ") !== FALSE;
				$is_del = strpos($mquery, " DELETE FROM ") !== FALSE;
				$is_edit = strpos($mquery, " UPDATE ") !== FALSE;
				
				if($is_add || $is_del || $is_edit) {
					$this->log_db_activity("{$query}");	
				}
							
				$this->log_db_activity($this->dbconn->info . "{{ $query} }");
				return $this->query_result;
			} else {				
				$strError .= "\n{$this->dbconn->connect_error}";
				$strError .= "\n{$this->dbconn->error}";
				$this->set_error($strError);
				if(!empty($this->log)) {
					$this->log_error($this->last_error);
				}
			}			
			
			
		} catch(Exception $e) {
			
			$this->set_error( 'Query Failed : ' . $e->getMessage()  . ' ' . $e->getFile()  . ' line ' . $e->getLine());
			if(!empty($this->log)) {
				$this->log_error($this->last_error);
			}	
						
		}	
		return false;	
	}	
	
	/**
	 * 
	 **/
	function log_db_activity($string) {
		if(!empty($this->log)) {
			$ts ='[ DB Activity ' . date('m-d-Y H:i:s') . ' ]';
			$this->log->insert($ts . $string);
		}	
	}	
	
	/**
	 * 
	 */
	function getLastError() {
		return $this->last_error;
	}
	
	/**
	 * 
	 **/
	function log_error($error_string) {
		$ts ='[ Error ' . date('m-d-Y H:i:s') . ' ]';
		if(!empty($this->log)) {
			$this->log->insert($ts . $error_string);
		}		
	}

	/**
	 * 
	 */
	function set_error($error_string) {
		$this->last_error = $error_string;
	}
	
	/**
	 * 
	 */
	function getNumRows() {
		if(!empty($this->query_result)) {
			return $this->query_result->num_rows;
		} else {
			return 0;
		}
	}
	
	/**
	 * 
	 */
	function last_id() {
		try {
			
			$this->query_result = $this->dbconn->query("SELECT LAST_INSERT_ID() AS last_id;");
			$o = $this->query_result->fetch_object();
			
			if($this->query_result)
				return $o->last_id;
			else {
				$strError .= "\n{$this->dbconn->connect_error}";
				$strError .= "\n{$this->dbconn->error}";
				$this->set_error($strError);
				if(!empty($this->log)) {
					$this->log_error($this->last_error);
				}
			}			
						
		} catch(Exception $e) {
			$this->set_error( 'Query Failed : ' . $e->getMessage()  . ' ' . $e->getFile()  . ' line ' . $e->getLine());
			if(!empty($this->log)) {
				$this->log_error($this->last_error);
			}			
		}
		return false;

	}
	
	/**
	 * 
	 */
	function affected_rows() {
		return $this->dbconn->affected_rows;
	}
	
	/**
	 * 
	 */
	function describe_table($tablename) {
		try {
			
			$this->query_result = $this->dbconn->query("SHOW FULL COLUMNS FROM `{$tablename}`");
			
			if($this->query_result)
				return $this->fetch($this->query_result,'all');
			else {
				$strError .= "\n{$this->dbconn->connect_error}";
				$strError .= "\n{$this->dbconn->error}";
				$this->set_error($strError);
				if(!empty($this->log)) {
					$this->log_error($this->last_error);
				}
			}
						
		} catch(Exception $e) {
			$this->set_error( 'Query Failed : ' . $e->getMessage()  . ' ' . $e->getFile()  . ' line ' . $e->getLine());
			if(!empty($this->log)) {
				$this->log_error($this->last_error);
			}			
		}
		return false;
		
	}
	
	/**
	 * 
	 */
	function quote($str) {
		return $this->dbconn->real_escape_string ($str);
	}	
	
	/**
	 * 
	 */
	function escape_string($str) {
		return $this->dbconn->real_escape_string ($str);	
	}
	
} 
	 
	
	 
?>