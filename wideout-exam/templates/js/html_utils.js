// js snippet for date validation
function checkvalid_date() {
	var ds_from = document.getElementById("s_date")
	var ds_to = document.getElementById("e_date")
	var dt_from =  ds_from.value.substring(3,5) + "/" + ds_from.value.substring(0,2) + "/" + ds_from.value.substring(6,10)
	dt_from =  new Date(dt_from).getTime()
	var dt_to =  ds_to.value.substring(3,5) + "/" + ds_to.value.substring(0,2) + "/" + ds_to.value.substring(6,10)
	dt_to =  new Date(dt_to).getTime()
	if(isNaN(dt_from )) {
		alert("Invalid Start Date")
		return false;
	}
	if(isNaN(dt_to)) {
		alert("Invalid End Date")
		return false;
	}
	if(dt_from > dt_to) {
		alert("Start Date must be earlier than the End Date")
		return false
	}

}

// enhance for version 2 transform automatically to mm/dd/yyyy format

function checkdates(dt_from, dt_to) {
	
	/* mm/dd/yyyy is most accepted date format for all IE browsers
	var ds_from = document.getElementById("s_date")
	var ds_to = document.getElementById("e_date")
	var dt_from =  ds_from.value.substring(3,5) + "/" + ds_from.value.substring(0,2) + "/" + ds_from.value.substring(6,10)
	dt_from =  new Date(dt_from).getTime()
	var dt_to =  ds_to.value.substring(3,5) + "/" + ds_to.value.substring(0,2) + "/" + ds_to.value.substring(6,10)
	dt_to =  new Date(dt_to).getTime()
	*/
	
	var dt_from =  new Date(dt_from).getTime()
	var dt_to =  new Date(dt_to).getTime()
	if(isNaN(dt_from )) {
		alert("Invalid Start Month/Year Period")
		return false;
	}
	if(isNaN(dt_to)) {
		alert("Invalid End Month/Year Period")
		return false;
	}
	if(dt_from > dt_to) {
		alert("Start Month/Year must be earlier than the End Month/Year")
		return false
	}
}

create_option(_filter_period_select, -1, "", false)
create_option(_filter_period_select, "yearly", "Yearly", true)				
function create_option(select_object, value, text, bln_selected) {
var option=document.createElement("option");
option.text=text;
option.value=value;
option.selected = bln_selected
try        
{				 
// for IE earlier than version 8            
select_object.add(option,select_object.options[null]);	
}				
catch (e)        
{	
select_object.add(option,null);        
}

//console.log('Value ' + value)
//console.log('Text ' + text)
//console.log(option)
//console.log('---------------------')
}


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script>
function displayResult()
{
var x=document.getElementById("mySelect");
var option=document.createElement("option");
option.text="Kiwi";
try
{
// for IE earlier than version 8
x.add(option,x.options[null]);
}
catch (e)
{
x.add(option,null);
}
}
</script>
</head>
<body>

<form>
<select id="mySelect">
<option>Apple</option>
<option>Pear</option>
<option>Banana</option>
<option>Orange</option>
</select>
</form>

<button type="button" onclick="displayResult()">Insert option</button>

</body>
</html> 


selectObject.add(option,before)

Parameter 	Description
option 	Required. Specifies the option to add. Must be an option or optgroup element
before 	Required. Where to insert the new option (null indicates that the new option will be inserted at the end of the list)





Browse - programming tips - javascript remove all options in a select

Date: 2008may8
Language: javaScript

Q.  How do I remove all the options from an HTML <select> ?

A.  Here is a function that does it.
Pass in the object handle of the <select> (not the options).

function removeOptions(obj) {
if (obj == null) return;
if (obj.options == null) return;
obj.options.length = 0;	 // That's it!
}

//Another way to do it:
function removeOptionsAlternate(obj) {
if (obj == null) return;
if (obj.options == null) return;
while (obj.options.length > 0) {
obj.remove(0);
}
}

//Example use:
function example() {
removeOptions(document.getElementById('myselect'));
}
