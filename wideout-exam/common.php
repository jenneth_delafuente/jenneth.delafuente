<?php
/********************************
* function
*********************************/

function is_logged_in() {
	return !empty($_SESSION['username']);
}
function phpbb_realpath($path)
{
	//global $nus_eng_root_path, $phpEx;

	return (!@function_exists('realpath') || !@realpath($nus_eng_root_path . 'includes/functions.'.$phpEx)) ? $path : @realpath($path);
}


function debug($testvar,$text='',$printvar=1) {
    if($printvar) {
        echo '<pre>',print_r($testvar, true), '</pre>';
    }
    
    if($testvar) {
         echo '<pre>',print_r($text, true), '</pre>';
    }
}

function db() {
	global $db;
	return $db;
}

// user basic view rights check
function is_view($pagename) {
	if(in_array($pagename,array('patient','data','file'))=== TRUE) {
		$checkview = 'view_' . $pagename ;	
		return $_SESSION[$checkview] > 0;
	} else
	{
		return true;
	}
}

// Logs of Processed
class File_Writer {
	public $classname = "File_Writer";

	// Root template directory.
	public $filename = 'default.log';

	/**
	 * Constructor. Simply sets the root dir.
	 *
	 */
	function File_Writer($root = LOGDIR)
	{
		$this->set_rootdir($root);
	}

	function insert_line($textline)
	{
		$filename = $this->root."/".$this->filename;
	 
		if (!$handle = fopen($filename, 'a+'))
		{
			return false;
		}
	
		if (fwrite($handle, $textline."\n") === FALSE)
		{
			return false;
		}
		
		fclose($handle);
		return true;
	}

	/**
	 * Sets the template root directory for this Template object.
	 */
	function set_rootdir($dir)
	{
		if (!is_dir($dir))
		{
			return false;
		}

		$this->root = $dir;
		return true;
	}

	/**
	 * Sets the template root directory for this Template object.
	 */
	function set_filename($filename)
	{
		$this->filename = $filename;
		return true;
	}
}

// user defined error handling function
/**
 * override php default error handler, this is effective to log errors during exceptions using try { } catch(Exception $e) { trigger_error($e->getMessage(),E_USER_ERROR); } 
 * 
 * OR a simple trigger_error() call
 * OR when PHP has caught errors during runtime 
 * executes on trigger_error() with E_USER_ERROR flag only
 */
function userErrorHandler($errno, $errmsg, $filename, $linenum, $vars ) 
{
 
    // timestamp for the error entry
    $dt = date("Y-m-d H:i:s (T)");

    // define an assoc array of error string
    // in reality the only entries we should
    // consider are E_WARNING, E_NOTICE, E_USER_ERROR,
    // E_USER_WARNING and E_USER_NOTICE
    $errortype = array (
                E_ERROR              => 'Error',
                E_WARNING            => 'Warning',
                E_PARSE              => 'Parsing Error',
                E_NOTICE             => 'Notice',
                E_CORE_ERROR         => 'Core Error',
                E_CORE_WARNING       => 'Core Warning',
                E_COMPILE_ERROR      => 'Compile Error',
                E_COMPILE_WARNING    => 'Compile Warning',
                E_USER_ERROR         => 'User Error',
                E_USER_WARNING       => 'User Warning',
                E_USER_NOTICE        => 'User Notice',
                E_STRICT             => 'Runtime Notice',
                E_RECOVERABLE_ERROR  => 'Catchable Fatal Error'
                );
    // set of errors for which a var trace will be saved
    
    $user_errors = array(E_ERROR, E_PARSE, E_CORE_ERROR, E_COMPILE_ERROR, E_USER_ERROR);
	
	$err = '['.$dt.']'." $errno $errortype[$errno] $errmsg $filename at Line $linenum ";
	
    if (in_array($errno, $user_errors)) {      
	   $err.= print_r($vars,true);
    }
    $err .= "\n";
    
    // save to the error log, and e-mail me if there is a critical user error
    // error_log($err, 3, "error.log");
    
	//$filename = 'error_logs_'.date('Ymd').'.txt';
	 
	$logfile = new File_Writer();
	$logfile->set_filename('errors_'.date("Ymd").'.log');	
	
	
	if (in_array($errno, $user_errors)) {	
		//$log_create_status = error_log($err,3,"./errors/{$filename}");
		//if(!$log_create_status) {
			//mail('developer@test.com','Critical User Error',$err);			 
		//}		
		$logfile->insert_line($err);
	}	
    if ($errno == E_USER_ERROR) {
       // mail("phpdev@example.com", "Critical User Error", $err);
    }
}


// ftp upload
function ftp_upload($remote_file, $upload_file, $mode=FTP_ASCII ) {		
	$connection = ftp_connect(FTP_SERVER);
	$login = ftp_login($connection, FTP_USER, FTP_PASSWORD);

	if (!$connection || !$login) { die('Connection attempt failed!'); }
	$upload = ftp_put($connection, $remote_file, $upload_file, $mode);

	if (!$upload) { echo 'FTP upload failed!'; }

	ftp_close($connection);
	
}

//params
function set_params($params=array(), $data=array(), $is_reset = false) {
	$vars = array();
	foreach($params as $index=>$name)	{
		
		if(!empty($data[$name])) {
				$vars[$name] = $data[$name];
		} else {
			if($_SERVER['REQUEST_METHOD'] == 'POST' || $_SERVER['REQUEST_METHOD'] == 'GET' ) {
				$vars[$name] = $_SERVER['REQUEST_METHOD'] == 'GET' ? $_GET[$name] : $_POST[$name];	
			}  		
		}
		
		if($is_reset) {
			$vars[$name] = '';
		}
	}
	
	return $vars; 
	 
}

// js inclusion
function add_js_file($js_file=array()) {
	global $template;
	$js_file = !is_array($js_file) ? array($js_file) : $js_file;
	foreach($js_file as $k=>$v) {
		$template->assign_block_vars('js_script', array('FILE' => $v));
	}
}

// css inclusion
function add_css_file($css_file=array()) {
	global $template;
	$css_file = !is_array($css_file) ? array($css_file) : $css_file;
	foreach($css_file as $k=>$v) {
		$template->assign_block_vars('css_file', array('FILE' => $v));
	}
}

// head html inclusion
function add_head($head_tags=array()) {
	global $HTML;	
	$head_tags = !is_array($head_tags) ? array($head_tags) : $head_tags;
	foreach($head_tags as $k=>$v) {
		$HTML['head'][] = $v;
	}	
}

// set_module_title
function set_module_title($msg='') {
	global $template;
	$msg = !is_array($msg) ? array($msg) : $msg;
	$msg = implode('<br/>',$msg);
	$template->assign_vars(array('MODULE_TITLE' => $msg));
}

// set_page_title
function set_page_title($msg='') {
	global $template;
	$msg = !is_array($msg) ? array($msg) : $msg;
	$msg = implode('<br/>',$msg);
	$template->assign_vars(array('PAGE_TITLE' => $msg));
}

// set_error_message
function set_error_message($msg='') {
	global $template;
	$msg = !is_array($msg) ? array($msg) : $msg;
	$msg = implode('<br/>',$msg);
	$template->assign_vars(array('ERROR_MESSAGE' => $msg));
}

// set_system_message
function set_system_message($msg='') {
	global $template;
	$msg = !is_array($msg) ? array($msg) : $msg;
	$msg = implode('<br/>',$msg);
	$template->assign_vars(array('SYSTEM_MESSAGE' => $msg));
}

// set_listing_title
function set_listing_title($msg='') {
	global $template;
	$msg = !is_array($msg) ? array($msg) : $msg;
	$msg = implode('<br/>',$msg);
	$template->assign_vars(array('LISTING_TITLE' => $msg));
}

//20150115 if module page wants to specify its own page layout
function set_page_layout($layout) {
	global $page_layout;
	$page_layout = $layout;
}

// get_template_files()
function &get_template_files() {
	global $template_files;
	return $template_files;
}

// base dir uri
function base_dir_uri() {
	$scheme = $_SERVER['REQUEST_SCHEME'];
	$server = $_SERVER['SERVER_NAME'];	
	$script_t = strtolower($_SERVER['SCRIPT_NAME']);	
	$script_t = explode('/index.php',$script_t);
	$script = $script_t[0];
	$query_string = $_SERVER['QUERY_STRING'];
	
	return "{$scheme}://{$server}/{$script}/";
}

// current url
function current_url() {
	$scheme = $_SERVER['REQUEST_SCHEME'];
	$server = $_SERVER['SERVER_NAME'];	
	$script = $_SERVER['SCRIPT_NAME'];
	$query_string = $_SERVER['QUERY_STRING'];
	
	return "{$scheme}://{$server}/{$script}?{$query_string}";	
}

// site_url
function site_url($query_params='') {
	$scheme = $_SERVER['REQUEST_SCHEME'];
	$server = $_SERVER['SERVER_NAME'];	
	$script = $_SERVER['SCRIPT_NAME'];
	
	return "{$scheme}://{$server}/{$script}?{$query_params}";	
}

?>