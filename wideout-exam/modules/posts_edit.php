<?php 
		set_page_layout('posts_admin');
		$table = new Table_Mysqli('posts', db(),$_REQUEST['id']);
		$form = new Table_Form($table);
		$form->onSubmit = 'onSubmit';
		$form->onSuccess = 'onSuccess_Edit';
		$form->addButton('back','Back to List','button');
		$form->setButtonAttribs('back', array('onclick'=>'window.location=\''.site_url('module=posts_list').'\'')) ;
		
		
		$form->setFormAttribs( array('enctype' => "multipart/form-data"));	
							
		$form_fields = new Form_Fields($form->getFormName(),false);		

		$form->fields['date_modified']['type'] = 'readonly';
		$form->fields['date_created']['type'] = 'readonly';

		$form->fields['new_featured_image']['type'] = 'file';
		$form->fields['new_featured_image']['title'] = 'Change Featured Image';
		$form->fields['featured_image']['type'] = 'readonly';			
		
		foreach($form->fields as $name=>$attr) {
			$form_fields->_addField($name, $attr['type'], $attr);
		}
				
		$form->run();
		$form->fields['featured_image']['value'] = '<div style="height:200px;width:90%;overflow:scroll;"><img src="'.FEATURED_IMG_DIR.$form->fields['featured_image']['value'].'"/></div>';
		$form->fieldsToHtml(); 
				
		
		if(!$form->is_ajax_request()) {
			
			ob_start();
		 	foreach($form->fields as $name => $attr) :
		 			
		 		if($attr['type'] == 'hidden') :
	 				echo $form->getFieldHtml($name); 
	 				continue;
		 		endif;
		 		if($name =='new_featured_image') {
		 			$title = '<div class="col-lg-1">'. $form->fields['new_featured_image']['title'] .'</div>';
		 		} else {
		 			$title = $form->getTitleHtml($name);
		 		}
	 		?>
				 
						<div id="fields-row" class="row">
			 				<?php echo $title; ?>
			 				<?php echo $form->getFieldHtml($name);?>
			 				<span class="field-error" id="<?php echo $name ?>-error"><?php echo $form->getError($name); ?></span>
			 			</div>
			 		 
								 		 
		 	<?php endforeach; ?>
						 		 	
		 	<?php
		 	$form_html = ob_get_contents();
		 	ob_end_clean();	
		  		
		
		$tpl_files =& get_template_files(); 
		$tpl_files['content'] = 'common/crud_form_bootstrap.html';
		set_page_title('Edit Post');
		$template->assign_vars(array('TITLE' => 'Edit Post'));		
		$template->assign_vars(	array(	
										'FORM_TAG'			=>  $form->openFormTag(),
										'FORM_HTML' 		=>	$form_html,
										'FORM_TITLE'		=>  'Edit Post',
										'FORM_NAME'			=>  $form->getFormName(),
										'SYSTEM_MESSAGE'	=>  $system_message,
										'SUCCESS_MESSAGE'	=>  $_REQUEST['success_message'],
										'FAILED_MESSAGE'	=>	($form->hasError() ? "Error saving the Post.<br/>" : '') .$form->getAllErrorsHtml(),
										'BUTTONS_HTML'		=> $form->buttonsHtml(),
										'JS_VALIDATION_CODE' => $form_fields->get_js_validation_code(),
										'JS_AJAX_SUBMIT_CODE' => $form_fields->get_js_ajax_submit_code()
				));		

		  							
		 
			 
		} else { // ajax request
				$response = array('test'=>$_FILES['featured_image']['tmp_name'],'server' => $_SERVER, 'post' => $_POST ,'success' => !$form->hasError(), 'success_msg' => 'Form submit success' , 'failed' => $form->hasError(), 'failed_msg' => ''.$form->getAllErrorsHtml());
				
			 	$err =array();
					
				foreach($form->fields as  $name=>$fld) {
					$err["{$name}"]= array('key'=> $name, 'text' =>$form->getError($name));
					
				}
				 
				$response['fieldErrors'] = $err;
				$response['success_url'] =  site_url('module=posts_list');
				echo json_encode($response);
				exit;			
			
		}	
		
/**
 * 
 * Enter description here ...
 * @param $form
 */
function onSubmit(&$form) {
	$size_info1 = getimagesize($_FILES['new_featured_image']['tmp_name']);

	if(!empty($size_info1)) {		
		
		//print_r($size_info1);
		//print_r($_FILES);
		//die();
		
		if( $size_info1[0] < 300 || $size_info1[1] < 300 
			|| $size_info1[0] > 680 || $size_info1[1]  > 680
		) {
			$form->setError('new_featured_image','New Featured Image must be between 300px to 680px');
		}
			
	 		
	}
}		

function onSuccess_Edit(&$form) {
	$size_info1 = getimagesize($_FILES['new_featured_image']['tmp_name']);
	print_r($_FILES);
	print_r(unlink(FEATURED_IMG_DIR . $_FILES['new_featured_image']['name']));
	
	if(!empty($size_info1)) {
		if(move_uploaded_file($_FILES['new_featured_image']['tmp_name'], FEATURED_IMG_DIR . $_FILES['new_featured_image']['name']))
		{
			 
			$id = $_REQUEST['id'];
			$form->table->update(array('featured_image' => $_FILES['new_featured_image']['name'], 'date_modified' =>  date('Y-m-d H:i:sa')), 
					array('id' => $id));
			//print_r($form->table->_dbError);
			$form->successUrl = site_url('module=posts_list&success_message=Data changed successfully.');
		}	
		else 
			die('Error transferring the image. Please check folder permissions');
	}		
}
?>