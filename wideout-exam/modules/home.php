<?php

//error_reporting(E_ALL);
set_page_layout('frontend');
require_once 'inc/home.inc.php';

	$cols = $filter = $sort = $fields = $attr = array();
	$table = new Table_Mysqli('posts', db());
	 
	$sort = array('expr' => 'date_created','order' => 'desc');
	 
	 
	foreach(array('post_title','author') as $name) {
		if(!empty($_REQUEST[$name])) {
			$filter[$name] = $name . " LIKE '%" . db()->escape_string( $_REQUEST[$name] ) . "%'";
		}
	}
	
	$iterator = new ListIterator_FrontEnd(db(),'posts', null,$filter,$sort,$link,$link_url);
	
	$iterator->add_column('post_title');
	$iterator->add_column('author');
	$iterator->add_column('date_created');
	$iterator->add_column('date_modified');
	
	$fields['post_title']['type'] = 'text';
	$fields['author']['type'] = 'text';

	$rows_per_page = empty($_REQUEST['rows_per_page']) ? 10 : $_REQUEST['rows_per_page']; 
	$listform = new ListForm_FrontEnd(db(),$iterator,$fields,$attr,$rows_per_page,10,'','post','option_name='.$_REQUEST['option_name']);	
	$listform->set_is_ajax(true);		
	$listform->onRow = 'Posts_onRow';
	$listform->setAddUrl("window.location='".site_url('module=posts_add') ."'");
	$listform->run(); 
	$reset_url = site_url('module=users_reset_password');
	
	$template->assign_vars(array('TITLE' => 'Backend'));
	
	$js_reset = <<<EOD
		<script>
			(function($){
				 				
			})(jQuery);
			
			
		</script>
EOD;
	
	add_head($js_reset);
	 // is ajax request
	if($listform->is_ajax_submit()) {
		$ret =  $listform->getListHtml();
		if($msg) {
			$ret['failed_message'] = $msg['failed_message'];
			$ret['success_message'] = $msg['success_message'];
		}
		echo json_encode($ret);
		exit;
	} else {		
		$tpl_files =& get_template_files(); 
		$tpl_files['content'] = 'common/list_bootstrap_frontend.html';
		set_page_title('Wide-Out Blog');		
		$template->assign_vars(	array(	'LISTFORM_HTML' 	=>  $listform->getListHtml(),
										'FORM_HTML' 		=>	$listform->getFormHtml(),
										'SYSTEM_MESSAGE'	=>  $system_message,
										'SUCCESS_MESSAGE'	=>  $_REQUEST['success_message'],
										'FAILED_MESSAGE'	=>	$_REQUEST['failed_message'],
										'BUTTONS_HTML'		=> $listform->getButtonsHtml()
				));
	}
	

/**
 * 
 * Enter description here ...
 * @param $row
 * @param $listform
 */
function Posts_onRow(&$row, &$listform) {
	$row['post_title'] = '<a title="Read the Article" href="' .  site_url('module=posts_view&id=' . $row['id']) . '">'.$row['post_title'] . '</a>';	 
}
	




// sample use : add css file to the template
// add_css_file($root_path.'skin/css/test.css');
// sample use : add js file to the template
// add_js_file($root_path.'js/test.js');
// sample use : add head entries to the template (array or string parameter)
// add_head('');

//set_system_message('this is a system message');

// set the title
$template->assign_vars(array('TITLE' => 'Home'));
?>