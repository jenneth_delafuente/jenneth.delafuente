<?php
		set_page_layout('frontend');
		$table = new Table_Mysqli('posts', db(),$_REQUEST['id']);

		$tpl_files =& get_template_files(); 
		$tpl_files['content'] = 'common/view_post_frontend.html';
		set_page_title($table->post_title);	

		$img = empty($table->featured_image) ? '' : ('<img src="'.FEATURED_IMG_DIR.$table->featured_image.'"/>');
		$template->assign_vars(	array(	
										'SYSTEM_MESSAGE'	=>  $system_message,
										'SUCCESS_MESSAGE'	=>  $_REQUEST['success_message'],
										'FAILED_MESSAGE'	=>	$_REQUEST['failed_message'],
										'DATE'				=>  date('F d, Y ', strtotime($table->date_created)),
										'AUTHOR'			=> $table->author,
										'POST_CONTENT'		=> $table->post_content,
										'IMAGE'				=> $img
				));		
?>