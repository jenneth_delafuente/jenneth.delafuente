<?php
	set_page_layout('posts_admin');	
	if($_REQUEST['action_delete'] == 1) {
		$msg = delete();		
	}
		 
	$cols = $filter = $sort = $fields = $attr = array();
	$table = new Table_Mysqli('posts', db());

	if(empty($_REQUEST['sort'])) {
		$sort = array('expr' => 'post_title','order' => 'asc');
	} else {
		$sort = array('expr' => $_REQUEST['sort'],'order' => $_REQUEST['order']);
	}
	 
	foreach(array('post_title','author') as $name) {
		if(!empty($_REQUEST[$name])) {
			$filter[$name] = $name . " LIKE '%" . db()->escape_string( $_REQUEST[$name] ) . "%'";
		}
	}
	
	$iterator = new List_Iterator_Mysqli(db(),'posts', null,$filter,$sort,$link,$link_url);
	
	$iterator->add_column('post_title');
	$iterator->add_column('author');
	$iterator->add_column('date_created');
	$iterator->add_column('date_modified');
	
	$fields['post_title']['type'] = 'text';
	$fields['author']['type'] = 'text';

	$rows_per_page = empty($_REQUEST['rows_per_page']) ? 10 : $_REQUEST['rows_per_page']; 
	$listform = new List_Form(db(),$iterator,$fields,$attr,$rows_per_page,10,'','post','option_name='.$_REQUEST['option_name']);	
	$listform->set_is_ajax(true);		
	$listform->onRow = 'Posts_onRow';
	$listform->setAddUrl("window.location='".site_url('module=posts_add') ."'");
	$listform->run(); 
	$reset_url = site_url('module=users_reset_password');
	
	$template->assign_vars(array('TITLE' => 'Backend'));
	
	$js_reset = <<<EOD
		<script>
			(function($){
				 				
			})(jQuery);
			
			
		</script>
EOD;
	
	add_head($js_reset);
	 // is ajax request
	if($listform->is_ajax_submit()) {
		$ret =  $listform->getListHtml();
		if($msg) {
			$ret['failed_message'] = $msg['failed_message'];
			$ret['success_message'] = $msg['success_message'];
		}
		echo json_encode($ret);
		exit;
	} else {		
		$tpl_files =& get_template_files(); 
		$tpl_files['content'] = 'common/list_bootstrap.html';
		set_page_title('Posts');		
		$template->assign_vars(	array(	'LISTFORM_HTML' 	=>  $listform->getListHtml(),
										'FORM_HTML' 		=>	$listform->getFormHtml(),
										'SYSTEM_MESSAGE'	=>  $system_message,
										'SUCCESS_MESSAGE'	=>  $_REQUEST['success_message'],
										'FAILED_MESSAGE'	=>	$_REQUEST['failed_message'],
										'BUTTONS_HTML'		=> $listform->getButtonsHtml()
				));
	}

	
 

/**
 * 
 * Enter description here ...
 * @param $row
 * @param $listform
 */
function Posts_onRow(&$row, &$listform) {
	$row['post_title'] = '<a title="Edit Post" href="' .  site_url('module=posts_edit&id=' . $row['id']) . '">'.$row['post_title'] . '</a>';	 
}

/**
 * 
 */
function delete() {
		$ids = explode('|', $_REQUEST['selected_ids']);

		if(is_array($ids)) {			 
			foreach($ids as $v) {
				if(empty($v)) continue;
				$ids_arr[] = $v;				
			}
			$ids = implode(',',$ids_arr);			 
		} else {
			$ids = $_REQUEST['selected_ids'];
		}	
		//print_r($ids);
		
		$query = 'DELETE FROM posts WHERE id IN ('. $ids . ')';
		//echo $query;
		db()->exec($query);
		
		if(!empty(db()->last_error)) {
			$ret['failed_message'] = 'Error deleting : '.db()->last_error ;				
		} else {
			$ret['success_message'] = 'Record successfully deleted';
		}
		
		return $ret;		
}
?>