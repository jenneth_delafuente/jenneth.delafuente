<?php
/**
 * - Routing page
 * - Module Inclusion
 * - Initialization / Bootstrap
 * - Template Engine initialization 
 * - Layout and template assignment
 */

error_reporting(E_ERROR);

require_once 'common.php';
require_once 'config.php';
require_once 'init_app.php';
require_once 'includes/class/Template.php';

$page_module = $_GET['module'];
$page_layout = @$_GET['layout']; 	// also allows you to define other layout file you desire
if(empty($page_module)) {
	$page_module = $default_module;
}

// access check 
if($page_module != 'login') {
	/* working, enable this later after login has been established
	require_once $root_path. '/includes/logincheck.php';
	*/ 
}

// check if there's an override to the default layout
if(empty($page_layout)) {
	$page_layout = 'default';
} else {
	if(!file_exists('layout/'.$page_layout.'.php')) {
		$page_layout = 'default';	
	}
}

// Template Instance
$root_template_path = 'templates/';
$template = new Template($root_template_path );

// module request , layout and content rendering
require_once 'modules/'.$page_module.'.php';

// no_layout is ideal for ajax request whose return value is json, 
// and others no visual layout needed 
if(@$_GET['layout'] != 'no_layout') {	
	require_once 'layout/'.$page_layout.'.php';
}

// retrieve current template assignments
$tpl_files =& get_template_files(); 
$content_template = empty($tpl_files['content']) ?  $page_module : $tpl_files['content'];


?>