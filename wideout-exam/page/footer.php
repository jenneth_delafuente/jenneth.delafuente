<?php
global $HTML;
$footer = $HTML['footer'];
$footer = !is_array($footer) ? array($footer) : $footer;
$template->assign_vars( array('FOOTER' => implode("\n\t",$footer)));
 

$tpl_files =& get_template_files();
if(!empty($tpl_files['footer'])) {
	$template->pparse('footer');  
}
?>