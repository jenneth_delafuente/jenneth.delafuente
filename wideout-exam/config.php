<?php
/***********************************
* Constants
************************************/
// LOG DIRECTORY
define('LOGDIR'		,'logs');
define('LIB_DIR'	, 'includes/library/');
define('BASEPATH'	, '.');

/************************************
* Setup Configurations
*************************************/
// DB DRIVERS : mysqli, mysql and pgsql for postgresql
$dbdriver = 'mysqli' ;

// DB CREDENTIALS
$dbhost = '127.0.0.1';
$dbname = 'wideoutdb';
$dbuser = 'root';
$dbpasswd = '';

// LOGIN SESSION TIMEOUT in MILLISECONDS
$session_timeout = -1;

// default page
$default_module = 'home';

/**********************************
* Template Variables
***********************************/
// may pass actual codes (js,html,css) or path to templates
$HTML['head']		= array();
$HTML['content'] 	= array();
$HTML['menu'] 	= array();
$HTML['sidemenu'] 	= array();
$HTML['sidemenu_left'] = array();
$HTML['sidemenu_right']	= array();
$HTML['top'] 		= array();
$HTML['footer'] 	= array();
$HTML['js']			= array();

/**************************
 * Directories
 **************************/ 
// featured image upload directory
define('FEATURED_IMG_DIR', 'www/upload/featured_image/');
?>