-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 16, 2015 at 03:12 AM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `wideoutdb`
--

CREATE DATABASE `wideoutdb`;
-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `post_title` varchar(100) NOT NULL,
  `post_content` text NOT NULL,
  `author` varchar(100) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `date_modified` datetime NOT NULL,
  `featured_image` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `post_title`, `post_content`, `author`, `date_created`, `date_modified`, `featured_image`) VALUES
(1, 'Trip to Carribean', 'this is the sample content', 'John Doe', '2015-01-15 13:05:11', '2015-01-15 14:05:11', 'kenshinxpotatocutter.jpg'),
(12, 'ssss', 's', 's', '2015-01-15 12:50:17', '2015-01-15 13:50:17', 'kenshinxpotatocutter.jpg');

-- --------------------------------------------------------
 
SET FOREIGN_KEY_CHECKS=1;